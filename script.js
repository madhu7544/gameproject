let startBtn = document.getElementById("start");
let moves = document.getElementById("moves");
let timer = document.getElementById("timer");
let gameContainer = document.getElementById("game-container");
let gameOver = document.getElementById("game-over");

let icons = [
  "lion",
  "lion",
  "owl",
  "owl",
  "penguin",
  "penguin",
  "rabbit",
  "rabbit",
  "rat",
  "rat",
  "tiger",
  "tiger",
  "zebra",
  "zebra",
  "pig",
  "pig",
];

//sorting icons
icons.sort(() => Math.random() - 0.5);

//creating each card
for (let i = 0; i < icons.length; i++) {
  let card = document.createElement("div");
  let front = document.createElement("img");
  let back = document.createElement("div");
  front.src = `data/${icons[i]}.png`;
  front.classList.add("front");
  back.classList.add("back");
  card.appendChild(back);
  card.appendChild(front);
  card.classList.add("card");
  gameContainer.appendChild(card);
}
let cards = document.querySelectorAll(".card");
console.log(cards);

let flippedCards = [];
let movesCount = 0;

//setting timer
let timeInterval;
let startTimer = () => {
  if (timeInterval) {
    return;
  }
  let count = 0;
  timeInterval = setInterval(() => {
    count++;
    timer.innerText = count;
  }, 1000);
};

//checking match the cards
function checkMatch() {
  let card1 = flippedCards[0];
  let card2 = flippedCards[1];
  if (card1.querySelector("img").src === card2.querySelector("img").src) {
    card1.removeEventListener("click", flipCard);
    card2.removeEventListener("click", flipCard);
    if (document.querySelectorAll(".flipped").length === cards.length) {
      startBtn.disabled = false;
      clearInterval(timeInterval);
      gameOver.textContent = "Game Over!!!";
    }
  } else {
    setTimeout(() => {
      card1.classList.remove("flipped");
      card2.classList.remove("flipped");
    }, 500);
  }
  flippedCards = [];
}


function flipCard() {
  if (flippedCards.length < 2 && !this.classList.contains("flipped")) {
    this.classList.add("flipped");
    flippedCards.push(this);
    movesCount++;
    moves.innerText = `${movesCount} Moves`;
    if (flippedCards.length === 2) {
      checkMatch();
    }
  }
}

startBtn.addEventListener("click", () => {
  startBtn.disabled = true;
  cards.forEach((card) => {
    card.classList.add("flipped");
    card.addEventListener("click", flipCard);
  });
  gameOver.textContent = "";
  setTimeout(() => {
    cards.forEach((card) => card.classList.remove("flipped"));
    startTimer();
  }, 2000);
});
